  elementTemplate = `
  <div class='panel panel-default portlet' id='$$DIVID$$'>
    <div class='panel-heading'>
      <h3 class='panel-title' id='$$PANELTITLE$$'>$$INPUTHEADING$$</h3>
      <h4>
        <span class='glyphicon glyphicon-minus' id='$$SPANID$$'></span>
        <span class='glyphicon glyphicon-pencil' id='$$PENCIL$$'></span>      
      </h4>
    </div>
    <div class='panel-body'>
      <input name='$$INPUTNAME$$' id='$$INPUTNAME$$'></input>
    </div>
  </div>
  `;

  checkBoxTemplate = `
  <div class='panel panel-default portlet' id='$$DIVID$$'>
    <div class='panel-heading'>
      <h3 class='panel-title' id='$$PANELTITLE$$'>$$INPUTHEADING$$</h3>
      <h4>
        <span class='glyphicon glyphicon-minus' id='$$SPANID$$'></span>
        <span class='glyphicon glyphicon-pencil' id='$$PENCIL$$'></span>
        <span class='glyphicon glyphicon-plus' id='$$ADDITEM$$'></span>
      </h4>
    </div>
    <div class='panel-body'>
      <div id="$$CHECKBOXUL$$">
        <div id="$$CHECKBOXITEMDIV$$_$$SEQ$$">
          <input type="checkbox" class="formcheckbox" name='$$INPUTNAME$$' id='$$INPUTNAME$$_$$SEQ$$'></input>
          <label for='$$INPUTNAME$$_$$SEQ$$' id='lbl_$$INPUTNAME$$_$$SEQ$$'>$$CHECKBOXSTUB$$</label>
          <h4>
            <span class='glyphicon glyphicon-minus' id='$$CHECKBOXITEMMINUS$$_$$SEQ$$'></span>
            <span class='glyphicon glyphicon-pencil' id='$$CHECKBOXITEMPENCIL$$_$$SEQ$$'></span>      
          </h4>          
        </div>
      </div>
    </div>
  </div>
  `;

    textAreaTemplate = `
  <div class='panel panel-default portlet' id='$$DIVID$$'>
    <div class='panel-heading'>
      <h3 class='panel-title' id='$$PANELTITLE$$'>$$INPUTHEADING$$</h3>
      <h4>
        <span class='glyphicon glyphicon-minus' id='$$SPANID$$'></span>
        <span class='glyphicon glyphicon-pencil' id='$$PENCIL$$'></span>      
      </h4>
    </div>
    <div class='panel-body'>
      <textarea name='$$INPUTNAME$$' id='$$INPUTNAME$$'></textarea>
    </div>
  </div>
  `;



  sortLayout = null;   

  var _utils = {
    _upperize: token => token[0].toUpperCase() + token.slice(1).toLowerCase(),
    _escapeRegExp: function (str) {
                      return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
                    }      

  }

    YUI().use(
  'aui-node', 'aui-datepicker', 'aui-alert',
  'aui-sortable-layout',
  function(Y) {
    function semaphore() {
        let init = 1;
        return function() {
            return init++;
        }
    }

    newId = semaphore();
    sectionId = semaphore();
    checkboxList = {};

    function updateSortable() {
        sortLayout = new Y.SortableLayout({
        dragNodes: '.portlet',
        dropNodes: '#formbox'
     });
    }

    function attachEditEvent(pencilID, panelID) {
      let currPencilTitle = document.getElementById(pencilID);
      currPencilTitle.addEventListener("click", function() {
        let currPanel = document.getElementById(panelID);
        let newTitle = prompt("Enter new title for field", currPanel.innerText);
        if (newTitle) {
          currPanel.innerHTML = currPanel.innerHTML.
                              replace(currPanel.innerText.trimRight(), newTitle);
        }
      });            
    }

    function attachDeleteEvent(minusID, parentDivID, superParentDivID) {
      let currMinus = document.getElementById(minusID);
      let parentDiv =document.getElementById(parentDivID);
      let superParentDiv = document.getElementById(superParentDivID);

      currMinus.addEventListener("click", function() {
        superParentDiv.removeChild(parentDiv);
      });

      updateSortable();
    }
      
    function parseNewWidget(template, operativeId, widgetMap) {
      let boxHtml = _.clone(template);

      let _normalizedWidget = _.object(_.zip(
                                _.map(_.keys(widgetMap), _utils._escapeRegExp), 
                                _.values(widgetMap)));

        _.each(_normalizedWidget, function(e, l, i) {
            let _regex = new RegExp(l, 'g');
            boxHtml = boxHtml.replace(_regex, e);
        });

      Y.one('#ignorethisdiv').placeBefore(boxHtml);
      updateSortable();

      attachDeleteEvent("span" + operativeId, "div" + operativeId, "formbox");
      attachEditEvent("pencil" + operativeId, "paneltitle" + operativeId);
    }


    function buildWidgetMap(prefix, operativeId, 
                            supplementaryWidgets, 
                            supplementaryWidgetValues) {
        let _baseWidgetElements = [
            "$$DIVID$$",
            "$$INPUTHEADING$$",
            "$$INPUTNAME$$",
            "$$SPANID$$",
            "$$PANELTITLE$$",
            "$$PENCIL$$"
        ];

        if (supplementaryWidgets) {
            _baseWidgetElements = _.union(_baseWidgetElements, supplementaryWidgets);
        }

        let _baseWidgetValues = [
            "div" + operativeId,
            _utils._upperize(prefix) + " " + operativeId,
            prefix.toLowerCase() + operativeId,
            "span" + operativeId,
            "paneltitle" + operativeId,
            "pencil" + operativeId
        ];

        if (supplementaryWidgetValues) {
            _baseWidgetValues = _.union(_baseWidgetValues, supplementaryWidgetValues);
        }

        return _.object(_.zip(_baseWidgetElements, _baseWidgetValues));
    }

    function parseCheckBoxWidget(template, operativeId, prefix) {
      let currId = operativeId;
      checkboxList[prefix + currId] = 1;

      let _checkBoxWidgets = ["$$CHECKBOXUL$$",
                                "$$SEQ$$",
                                "$$CHECKBOXSTUB$$",
                                "$$ADDITEM$$",
                                "$$CHECKBOXITEMDIV$$",
                                "$$CHECKBOXITEMMINUS$$",
                                "$$CHECKBOXITEMPENCIL$$"];

      let _checkBoxWidgetValues = [prefix + "ul" + currId,
                                    checkboxList[prefix + currId],
                                    _utils._upperize(prefix) + " Item " + checkboxList[prefix + currId],
                                    prefix + "additem" + currId,
                                    prefix + "itemdiv" + currId,
                                    prefix + "minus" + currId,
                                    prefix + "pencil" + currId];

      parseNewWidget(template, currId, 
                    buildWidgetMap(prefix, currId, _checkBoxWidgets, 
                    _checkBoxWidgetValues));


      attachEditEvent(prefix + "pencil" + currId + "_" + checkboxList[prefix + currId], 
                      "lbl_" + prefix + currId + "_" + checkboxList[prefix + currId]);

      checkboxList[prefix + currId] = checkboxList[prefix + currId] + 1;

      let currAddItem = document.getElementById(prefix + "additem" + currId);
      currAddItem.addEventListener("click", function() {
        let activeID = currAddItem.id.match(/[0-9]+/)[0];
        firstCheckBoxBoundingDiv =document.getElementById(prefix + "itemdiv" + activeID + "_1");
        newCheckBox = Y.one("#" + firstCheckBoxBoundingDiv.id).clone().outerHTML();
        newCheckBox = newCheckBox.replace(/_1/g, "_" + checkboxList[prefix + activeID]).
                                  replace(/Item [0-9]+/, "Item " + checkboxList[prefix + activeID]);
        Y.Node.create(newCheckBox).appendTo("#" + prefix + "ul" + currAddItem.id.match(/[0-9]+/)[0]);
        attachEditEvent(prefix + "pencil" + activeID + "_" + checkboxList[prefix + activeID], 
                      "lbl_" + prefix + activeID + "_" + checkboxList[prefix + activeID]);
        attachDeleteEvent(prefix + "minus" + activeID + "_" + checkboxList[prefix + activeID],
                        prefix + "itemdiv" + activeID + "_" + checkboxList[prefix + activeID],
                        prefix + "ul" + activeID);
                      
        checkboxList[prefix + activeID] = checkboxList[prefix + activeID] + 1;
      });      
    }

    document.getElementById('addinput')
    .addEventListener("click", function() {
      currId = newId();
      parseNewWidget(elementTemplate, currId, 
                    buildWidgetMap("input", currId));
    });

    document.getElementById('addtextarea')
    .addEventListener("click", function() {
      currId = newId();
      parseNewWidget(textAreaTemplate, currId, 
                    buildWidgetMap("textarea", currId));
      CKEDITOR.replace("textarea" + currId);
    });

    document.getElementById('adddate')
    .addEventListener("click", function() {
      currId = newId();
      parseNewWidget(elementTemplate, currId, 
                    buildWidgetMap("date", currId));
      new Y.DatePicker(
      {
        trigger: '#' + 'div' + currId + ' input',
        popover: {
          zIndex: 1
        },
        on: {
          selectionChange: function(event) {
            console.log(event.newSelection)
          }
        }
      }
    );
    });

    document.getElementById("addcheckbox")
    .addEventListener("click", function() {
      currId = newId();
      parseCheckBoxWidget(checkBoxTemplate, currId, "checkbox");
    });

    document.getElementById("addradio")
    .addEventListener("click", function() {
      currId = newId();
      let radioTemplate = checkBoxTemplate.replace('type="checkbox"', 'type="radio"')
      parseCheckBoxWidget(radioTemplate, currId, "radio");
    });

   sectionButton = document.getElementById('addsection');
   sectionButton.addEventListener("click", function() {
      let currSectionId = sectionId();
      sectionList =document.getElementById("sections");
      let newOption = document.createElement("option");
      newOption.text = "Section " + currSectionId;
      sectionList.add(newOption);

      new Y.Alert(
        {
          boundingBox: "#alertBox",
          bodyContent: "Section " + currSectionId + " added to form.",
          closeable: true,
          cssClass: 'alert-info',
          render: true
        }
      );
   });

   updateSortable();

  }
);
